package com.ravesh.poc.grpc.server;

import io.grpc.*;


public class GrpcAuthorizationInterceptor implements ServerInterceptor {

    public GrpcAuthorizationInterceptor() {
    }

    @Override
    public <ReqT, RespT> ServerCall.Listener<ReqT> interceptCall(ServerCall<ReqT, RespT> call, Metadata headers, ServerCallHandler<ReqT, RespT> next) {
        if (!tokenIsValid(headers)) {
            call.close(Status.UNAUTHENTICATED.withDescription("Invalid token"), headers);
            return new ServerCall.Listener<ReqT>() {
            };
        }
        return next.startCall(call, headers);
    }

    private boolean tokenIsValid(Metadata headers) {
        // Add your logic to validate the token here
        // For example, you can check the token against a database or perform any other authentication checks
        String tokenValue = headers.get(Metadata.Key.of("Authorization", Metadata.ASCII_STRING_MARSHALLER));
        // Return true if the token is valid, otherwise false
        return isValid(tokenValue);
    }

    private boolean isValid(String tokenValue) {
        // Add your token validation logic here
        // Return true if the token is valid, otherwise false
        return "valid_token".equals(tokenValue);
    }
}

