package com.ravesh.poc.grpc.server;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.ServerInterceptors;

public class GreeterServer {
    public static void main(String[] args) throws Exception {
        Server server = ServerBuilder.forPort(50051)
                //.addService(new GreeterImpl())
                .addService(ServerInterceptors.intercept(new GreeterServiceImpl(), new GrpcAuthorizationInterceptor()))
                .build();

        server.start();
        System.out.println("Server started!");

        server.awaitTermination();
    }
}

