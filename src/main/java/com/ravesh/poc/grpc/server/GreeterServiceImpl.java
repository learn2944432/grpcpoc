package com.ravesh.poc.grpc.server;

import com.ravesh.poc.grpc.GreeterGrpc;
import com.ravesh.poc.grpc.GreeterProto;
import io.grpc.stub.StreamObserver;

public class GreeterServiceImpl extends GreeterGrpc.GreeterImplBase {

    @Override
    public void sayHello(GreeterProto.HelloRequest request, StreamObserver<GreeterProto.HelloResponse> responseObserver) {
        String name = request.getName();
        String message = "Hello, " + name + "!";

        GreeterProto.HelloResponse response;
        for (int i = 1; i < 6; i++) {
            response = GreeterProto.HelloResponse.newBuilder()
                    .setMessage(message + " " + i)
                    .build();
            responseObserver.onNext(response);
        }

        responseObserver.onCompleted();
    }
}
