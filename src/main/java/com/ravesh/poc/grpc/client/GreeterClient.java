package com.ravesh.poc.grpc.client;

import com.ravesh.poc.grpc.GreeterGrpc;
import com.ravesh.poc.grpc.GreeterProto.HelloRequest;
import com.ravesh.poc.grpc.GreeterProto.HelloResponse;
import io.grpc.ClientInterceptors;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

import java.util.Iterator;

public class GreeterClient {
    public static void main(String[] args) {
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 50051)
                .usePlaintext()
                .build();

        GreeterGrpc.GreeterBlockingStub stub = GreeterGrpc.newBlockingStub(ClientInterceptors.intercept(channel, new ClientAuthInterceptor(getToken())));

        HelloRequest request = HelloRequest.newBuilder()
                .setName("Avni Ravesh")
                .build();

        System.out.println("Request to be sent");
        Iterator<HelloResponse> respItr = stub.sayHello(request);
        respItr.forEachRemaining(t -> System.out.println("Response received from server: " + t.getMessage()));

        channel.shutdown();
    }

    public static String getToken() {
        return "valid_token";
    }
}

