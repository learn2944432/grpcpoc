package com.ravesh.poc.grpc.client;

import com.ravesh.poc.grpc.GreeterGrpc;
import com.ravesh.poc.grpc.GreeterProto.HelloRequest;
import com.ravesh.poc.grpc.GreeterProto.HelloResponse;
import io.grpc.*;

import java.util.Iterator;
import java.util.concurrent.Executor;

public class GreeterClientWithCreds {
    public static void main(String[] args) {
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 50051)
                .usePlaintext()
                .build();

        CallCredentials credentials = new CallCredentials() {
            @Override
            public void applyRequestMetadata(RequestInfo requestInfo, Executor executor, MetadataApplier metadataApplier) {
                Metadata metadata = new Metadata();
                metadata.put(Metadata.Key.of("Authorization", Metadata.ASCII_STRING_MARSHALLER), getToken());
                metadataApplier.apply(metadata);
            }

            @Override
            public void thisUsesUnstableApi() {

            }
        };

        GreeterGrpc.GreeterBlockingStub stub = GreeterGrpc.newBlockingStub(channel)
                .withCallCredentials(credentials);

        HelloRequest request = HelloRequest.newBuilder()
                .setName("Avni Ravesh")
                .build();

        System.out.println("Request to be sent with Creds");
        Iterator<HelloResponse> respItr= stub.sayHello(request);

        respItr.forEachRemaining(t -> System.out.println("Response received from server: " + t.getMessage()));

        channel.shutdown();
    }

    public static String getToken() {
        return "valid_token";
    }
}

